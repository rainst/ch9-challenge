var index = {
  index: (req, res) => {
    res.json('server is up');
  },
  
  filter_drm_shows: (req, res, next) => {
    var shows = req.body.payload || [];
    var filteredShows = shows.filter(show => {return (show.episodeCount > 0 && show.drm)})
      .map(show => {return {image: show.image.showImage, slug: show.slug, title: show.title}});

    var response = {response: filteredShows};
    res.json(response);
  }
}

module.exports = index;