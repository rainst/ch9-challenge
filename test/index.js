process.env.ch9_testing = true;

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../bin/www');
var should = chai.should();
var requestbody = require('./mock-request.json');

chai.use(chaiHttp);

describe('/POST filter_drm_shows', () => {
  it('Empty request: it should return an object with \'response\' being an empty array', (done) => {
    chai.request(server)
      .post('/filter_drm_shows')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object').have.property('response').be.a('array');
        res.body.response.length.should.be.eql(0);
        done();
      });
  });

  it('Mock request: it should filter and return all the 7 shows with drm and with at least one episode', (done) => {
    chai.request(server)
      .post('/filter_drm_shows')
      .type('json')
      .send(requestbody)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object').have.property('response').be.a('array');
        res.body.response.length.should.be.eql(7);
        res.body.response.every(element => {
          return (element.image != undefined && element.slug != undefined && element.title != undefined);
        }).should.be.eql(true);
        done();
      });
  });

  it('Malformed request: it should return a 400 malformed request http status and an object with an error message', (done) => {
    chai.request(server)
      .post('/filter_drm_shows')
      .type('json')
      .send('{"malformed" "json"}')
      .end((err, res) => {
        res.should.have.status(400);
        res.body.should.be.a('object').have.property('error').be.a('string');
        done();
      });
  });
});