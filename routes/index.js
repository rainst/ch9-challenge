var express = require('express');
var router = express.Router();
var controller = require('../controllers');

router.get('/', controller.index);

router.post('/filter_drm_shows', controller.filter_drm_shows);

module.exports = router;
